import {combineReducers} from 'redux';
import {dashboard_reducer} from './dashboard/reducers';

export default combineReducers({
    dashboard: dashboard_reducer
});
