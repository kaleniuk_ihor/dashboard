export const ACTION_SET_TABLE_DATA = "ACTION_SET_TABLE_DATA";
export const ACTION_SET_GRAPH_DATA = "ACTION_SET_GRAPH_DATA";

export const setTableData = table => ({
    type: ACTION_SET_TABLE_DATA,
    payload: table
});

export const setGraphData = graph => ({
    type: ACTION_SET_GRAPH_DATA,
    payload: graph
});