import {
    ACTION_SET_TABLE_DATA,
    ACTION_SET_GRAPH_DATA
} from './actions';

const dashboardState = {
    table: null,
    graph: null
};

export const dashboard_reducer = (state = dashboardState, action) => {
    switch (action.type) {
        case ACTION_SET_TABLE_DATA:
            return {
                ...state,
                table: action.payload
            };

        case ACTION_SET_GRAPH_DATA:
            return {
                ...state,
                graph: action.payload
            };

        default:
            return state
    }
}
