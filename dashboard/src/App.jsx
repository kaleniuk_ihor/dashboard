import React from 'react';
import TableContainer from './containers/table.Container';
import GraphContainer from './containers/graph.Container';

class MainPageContainer extends React.Component {
    render() {
        return <div id={"dashboard"}>
            <div className={"body-block"}>
                <GraphContainer/>
            </div>
            <div className={"body-block"}>
                <TableContainer/>
            </div>

        </div>
    }
}


export default MainPageContainer;
