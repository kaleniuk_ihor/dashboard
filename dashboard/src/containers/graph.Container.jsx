import React from 'react';
import {connect} from 'react-redux';
import Graph from '../components/graph'
import {setGraphData} from '../store/dashboard/actions';
import GraphData from "./static/layaout";

class GraphContainer extends React.Component {
    componentDidMount() {
        this.props.setGraphData(GraphData)
    }

    render() {
        return <Graph
            data={this.props.graph}
        />
    }
}

const mapStateToProps = state => {
    return {
        graph: state.dashboard.graph
    };
};

const mapDispatchProps = {
    setGraphData
}

export default connect(mapStateToProps, mapDispatchProps)(GraphContainer);
