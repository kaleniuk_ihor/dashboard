import React from 'react';
import {connect} from 'react-redux';
import {setTableData} from '../store/dashboard/actions';
import TableData from './static/table';
import Table from '../components/table';

class TableContainer extends React.Component {
    componentDidMount() {
        this.props.setTableData(TableData);
    }

    render() {
        return (this.props.table !== null) ? <Table
            data={this.props.table}
        /> : null
    }
}

const mapStateToProps = state => {
    return {
        table: state.dashboard.table
    };
};

const mapDispatchProps = {
    setTableData
};

export default connect(mapStateToProps, mapDispatchProps)(TableContainer);
