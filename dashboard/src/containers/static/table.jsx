const TableData = {
    columns: [
        {title: '№', field: 'number'},
        {title: 'Время', field: 'time'},
        {title: 'Длительность', field: 'length'},
        {title: 'Кр', field: 'kr'},
        {title: 'Икс', field: 'ex'},
        {title: 'Внш', field: 'vnh'},
        {title: 'Текст события', field: 'text'},
        {title: 'Кс', field: 'ks'},
        {title: 'OMI', field: 'omi'}
    ],
    data: [
        {
            number: 0,
            time: '16 Июня 17:05',
            length: '0:30',
            kr: true,
            ex: true,
            vnh: '',
            text: 'Проверка: Доступность WMI bankusers.domain.ru. Метрика: OSNname превысила порог «Osname contains «Microsoft» со значением «Microsoft Windows Server 2008 R2 Enterprise» на bankusers.domain.ru',
            ks: true,
            omi: true
        },
        {
            number: 1,
            time: '16 Июня 17:30',
            length: '1:45',
            kr: false,
            ex: false,
            vnh: '',
            text: 'Проверка: Доступность WMI bankusers.domain.ru. Метрика: OSNname превысила порог «Osname contains «Microsoft» со значением «Microsoft Windows Server',
            ks: true,
            omi: true
        },
        {
            number: 2,
            time: '16 Июня 17:45',
            length: '1:45',
            kr: true,
            ex: true,
            vnh: '',
            text: 'Проверка: Доступность WMI bankusers.domain.ru.',
            ks: true,
            omi: true
        },
        {
            number: 3,
            time: '16 Июня 17:45',
            length: '1:45',
            kr: false,
            ex: false,
            vnh: '',
            text: 'Проверка: Доступность WMI bankusers.domain.ru. Метрика: OSNname превысила порог «Osname contains «Microsoft» со значением «Microsoft Windows Server 2008 R2 Enterprise» ',
            ks: true,
            omi: true
        },
        {
            number: 4,
            time: '16 Июня 17:45',
            length: '1:45',
            kr: true,
            ex: false,
            vnh: '',
            text: 'Проверка: Доступность WMI bankusers.domain.ru. Метрика: OSNname превысила порог «Osname contains «Microsoft» со значением «Microsoft Windows Server 2008 R2 Enterprise» на bankusers.domain.ru',
            ks: true,
            omi: true
        },
        {
            number: 5,
            time: '16 Июня 17:45',
            length: '1:45',
            kr: true,
            ex: false,
            vnh: '',
            text: 'Проверка: Доступность WMI bankusers.domain.ru. Метрика: OSNname превысила ',
            ks: true,
            omi: true
        },
        {
            number: 6,
            time: '16 Июня 17:45',
            length: '1:45',
            kr: true,
            ex: false,
            vnh: '',
            text: 'Проверка: Доступность WMI bankusers.domain.ru. Метрика: OSNname превысила ',
            ks: true,
            omi: true
        },
        {
            number: 7,
            time: '16 Июня 17:45',
            length: '1:45',
            kr: true,
            ex: false,
            vnh: '',
            text: 'Проверка: Доступность WMI bankusers.domain.ru. Метрика: OSNname превысила ',
            ks: true,
            omi: true
        },
        {
            number: 8,
            time: '16 Июня 17:45',
            length: '1:45',
            kr: true,
            ex: false,
            vnh: '',
            text: 'Проверка: Доступность WMI bankusers.domain.ru. Метрика: OSNname превысила ',
            ks: true,
            omi: true
        },
        {
            number: 9,
            time: '16 Июня 17:45',
            length: '1:45',
            kr: true,
            ex: false,
            vnh: '',
            text: 'Проверка: Доступность WMI bankusers.domain.ru. Метрика: OSNname превысила ',
            ks: true,
            omi: true
        },
        {
            number: 10,
            time: '16 Июня 17:45',
            length: '1:45',
            kr: true,
            ex: false,
            vnh: '',
            text: 'Проверка: Доступность WMI bankusers.domain.ru. Метрика: OSNname превысила ',
            ks: true,
            omi: true
        }
    ]
};

export default TableData