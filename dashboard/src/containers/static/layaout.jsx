const points = [3, 2, 4, 1, 3, 5, 1, 2, 3, 5, 4, 5, 3, 4, 5];
const line = ["15:30", "16:00", "16:30", "17:00", "17:30", "18:00", "18:30", "19:00"];

const randomInteger = (min, max) => {
    let rand = min - 0.5 + Math.random() * (max - min + 1);
    return Math.round(rand);
};

const prepareRow = (points, key) => {
    return [...points].map(item => {
        return item + key + randomInteger(0, 750);
    });
};

const Hardcode_data = [
    {
        y: prepareRow(points, 6500),
        x: line,
        mode: 'lines',
        fill: 'tozeroy',
        type: 'scatter',
        fillcolor: 'rgba(7, 104, 159, 0.3)',
        line: {
            shape: 'linear',
            color: '#07689F',
            width: 3
        }
    },
    {
        y: prepareRow(points, 2300),
        x: line,
        mode: 'lines',
        fill: 'tozeroy',
        type: 'scatter',
        fillcolor: 'rgba(7, 159, 150, 0.4)',
        line: {
            shape: 'linear',
            color: '#079F96',
            width: 3
        },
    },
    {
        y: prepareRow(points, 1900),
        x: line,
        mode: 'lines',
        fill: 'tozeroy',
        type: 'scatter',
        fillcolor: ' rgba(242, 158, 31, 0.5)',
        line: {
            shape: 'linear',
            color: '#F29E1F',
            width: 3
        },
    },
    {
        y: prepareRow(points, 300),
        x: line,
        mode: 'lines',
        fill: 'tozeroy',
        type: 'scatter',
        fillcolor: '#87437C',
        line: {
            shape: 'linear',
            color: '#9B4C8E',
            width: 3
        },
    },
];

export default Hardcode_data