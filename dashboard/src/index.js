import React from 'react';
import ReactDOM from 'react-dom';

import {createStore} from 'redux';
import {Provider} from 'react-redux';

import './index.scss';
import App from './App';
import * as serviceWorker from './serviceWorker';

import rootReducer from './store/reducer';

const store = createStore(rootReducer);

ReactDOM.render(<Provider store={store}>
    <App/>
</Provider>, document.getElementById('root'));

serviceWorker.unregister();
