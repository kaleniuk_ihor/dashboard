import React from 'react';
import Plot from 'react-plotly.js';
import Layaout from './data/layaout';

class Graph extends React.Component {
    render() {
        return <Plot
            data={this.props.data}
            layout={Layaout}
            config={{displayModeBar: false}}
        />

    }
}

export default Graph