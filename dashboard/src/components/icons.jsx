import React from 'react';

class Icons extends React.Component {
    render() {
        return <svg className={"icon icon-" + this.props.icon + " " + this.props.style}
                    style={this.props.color && {color: this.props.color}}>
            <use xlinkHref={"#icon-" + this.props.icon}></use>
        </svg>
    }
}

export default Icons