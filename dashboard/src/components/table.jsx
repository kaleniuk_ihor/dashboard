import React from 'react';
import {withStyles, makeStyles} from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Toolbar from '@material-ui/core/Toolbar';
import Paper from '@material-ui/core/Paper';
import Icons from './icons';

const StyledTableCell = withStyles(theme => ({
    head: {
        // backgroundColor: '#313b4d',
        color: '#5784AC',
        borderBottom: 'none',
        fontSize: 11,
    },
    body: {
        fontSize: 11,
        color: "#798699",
        borderBottom: 'none'
    },
}))(TableCell);

const StyledTableRow = withStyles(theme => ({
    root: {
        '&:nth-of-type(odd)': {
            backgroundColor: "#273345",
        },
    },
}))(TableRow);

const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
        overflowX: 'auto',
        backgroundColor: 'rgba(255, 255, 255, 0.01)'
    },
    title: {
        color: '#fff',
        fontFamily: 'sans-serif',
        fontSize: '12px',
        marginLeft: '-15px',
        minHeight: "auto",
        padding: "27px 0 7px 23px"
    },
    table: {
        minWidth: 700,
        borderBottom: 'none'
    },
    tableWrapper: {
        maxHeight: 290,
        overflow: 'auto',
    },
    row: {
        whiteSpace: 'pre'
    },
    icon: {
        fontSize: 16
    }
}));

export default function CustomizedTables(props) {
    const classes = useStyles();
    return (
        <Paper className={classes.root}>
            <Toolbar className={classes.title}>
                События банк-онлайн за последние 24 часа
            </Toolbar>
            <div className={classes.tableWrapper}>
                <Table className={classes.table} size="small" aria-label="table">
                    <TableHead>
                        <TableRow>
                            {props.data.columns.map((row, key) => (
                                <StyledTableCell key={key} align="center">{row.title}</StyledTableCell>
                            ))}
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {props.data.data.map(row => (
                            <StyledTableRow key={row.number}>
                                <StyledTableCell component="th" scope="row">{row.number}</StyledTableCell>
                                <StyledTableCell align="center" className={classes.row}>{row.time}</StyledTableCell>
                                <StyledTableCell align="center">{row.length}</StyledTableCell>
                                <StyledTableCell align="center">{(row.kr) ?
                                    <Icons style={classes.icon} color={"#688C2E"} icon={"checked"}/> :
                                    <Icons style={classes.icon} icon={"question"}/>}</StyledTableCell>
                                <StyledTableCell align="center">{row.ex &&
                                <Icons style={classes.icon} icon={"coffee"}/>}</StyledTableCell>
                                <StyledTableCell align="center">{row.vnh}</StyledTableCell>
                                <StyledTableCell align="left">{row.text}</StyledTableCell>
                                <StyledTableCell align="center">{row.ks &&
                                <Icons style={classes.icon} icon={"info"}/>}</StyledTableCell>
                                <StyledTableCell align="center">{row.omi &&
                                <Icons style={classes.icon} icon={"bell"}/>}</StyledTableCell>
                            </StyledTableRow>
                        ))}
                    </TableBody>
                </Table>
            </div>
        </Paper>
    );
}
