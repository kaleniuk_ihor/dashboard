const Layaout = {
    title: {
        text: 'Количевство транзакций, шт.',
        font: {
            size: 12,
            color: "white",
            fontFamily: 'Gothem_pro'
        },
        xref: 'left',
        x: 0.01,
        y: 0.96
    },
    margin: {
        l: 20,
        r: 46,
        b: 28,
        t: 20,
        pad: 4
    },
    width: 800,
    height: 256,
    paper_bgcolor: 'rgba(255, 255, 255, 0.01)',
    plot_bgcolor: 'rgba(0,0,0,0)',
    showlegend: false,
    autosize: false,
    xaxis: {
        autotick: true,
        ticks: 'outside',
        tickcolor: 'transparent',
        gridcolor: '#34455F',
        side: "bottom",
        position: 0.005,
        tickfont: {
            family: 'Arial, sans-serif',
            size: 9,
            color: '#D9E1EB'
        }
    },
    yaxis: {
        autotick: true,
        ticks: 'outside',
        tickcolor: 'transparent',
        tickformat: 's',
        gridcolor: '#34455F',
        side: "right",
        position: 0.999,
        tickfont: {
            family: 'Arial, sans-serif',
            size: 9,
            color: '#D9E1EB'
        },
    }
};

export default Layaout